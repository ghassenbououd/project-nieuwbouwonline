<?php

namespace app\controllers;

use Yii;
use app\models\Houses;
use app\models\HousesSearch;

class CalculateController extends \yii\web\Controller
{
    public function actionCalculate()
    {
        return $this->render('calculate');
    }

    public function actionPost()
    {

        $request = Yii::$app->request;
        $data = $request->post();

        $floor = $data["square_meter"];
        $postalcode = $data["postalcode"];

        $houses = $this -> getHouses($floor, $postalcode);

        if( count($houses) != 10 ){
            $houses = $this -> getHouses($floor, substr($postalcode, 0, 4));
            if( count($houses) != 10 ){
                $houses = $this -> getHouses($floor, substr($postalcode, 0, 3));
            }
            if( count($houses) != 10 ){
                $houses = $this -> getHouses($floor, substr($postalcode, 0, 2));
            }

        }

        if( count($houses) ){
            if( count($houses) < 10 )
                Yii::$app->session->setFlash('notice', "we found less than 10 houses");

            $total_price = 0;
            $total_floor = 0;

            foreach ($houses as $house){
                $total_price += $house["price"];
                $total_floor += $house["floor"];
            }

            return $this->render('calculatepost', [
                'houses' => $houses,
                'total_price' => $total_price,
                'total_floor' => $total_floor,
            ]);
        }else{
            Yii::$app->session->setFlash('error', "No items found");
            return $this->redirect(['calculate']);
        }
    }

    private function getHouses($floor, $postalcode){

        $houses = Houses::find()
            ->where(['like', 'postalcode', $postalcode . '%', false])
            ->andWhere(['floor' => $floor])
            ->limit(10)
            ->all()
        ;
        return $houses;
    }

}
