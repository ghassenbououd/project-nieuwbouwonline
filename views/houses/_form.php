<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Houses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="houses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'house_id')->textInput() ?>

    <?= $form->field($model, 'streetname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'housenumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'addition')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postalcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'floor')->textInput() ?>

    <?= $form->field($model, 'parcel')->textInput() ?>

    <?= $form->field($model, 'nbr_rooms')->textInput() ?>

    <?= $form->field($model, 'broker_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'broker_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hose_img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hose_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_ins')->textInput() ?>

    <?= $form->field($model, 'date_upd')->textInput() ?>

    <?= $form->field($model, 'object_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
