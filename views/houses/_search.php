<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HousesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="houses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'house_id') ?>

    <?= $form->field($model, 'streetname') ?>

    <?= $form->field($model, 'housenumber') ?>

    <?= $form->field($model, 'addition') ?>

    <?= $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'postalcode') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'parcel') ?>

    <?php // echo $form->field($model, 'nbr_rooms') ?>

    <?php // echo $form->field($model, 'broker_name') ?>

    <?php // echo $form->field($model, 'broker_id') ?>

    <?php // echo $form->field($model, 'hose_img') ?>

    <?php // echo $form->field($model, 'hose_url') ?>

    <?php // echo $form->field($model, 'date_ins') ?>

    <?php // echo $form->field($model, 'date_upd') ?>

    <?php // echo $form->field($model, 'object_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
