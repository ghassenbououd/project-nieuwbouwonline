<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<blockquote>
    <p>Average floor price: <?php echo ceil($total_price / count($houses)) ?></p>
    <p>Average m² price: <?php echo ceil($total_price / $total_floor) ?></p>
</blockquote>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Street Name</th>
                <th>House Number</th>
                <th>City</th>
                <th>Postal Code</th>
                <th>Price</th>
                <th>Floor</th>
                <th class="price-m2">Price m²</th>
                <th>Parcel</th>
                <th>Nbr Rooms</th>
                <th>Broker Name</th>
                <th>Image</th>
                <th>Hose URL</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($houses as $house): ?>
            <tr>
                <td><?php echo $house["streetname"] ?></td>
                <td><?php echo $house["housenumber"] ?></td>
                <td><?php echo $house["city"] ?></td>
                <td><?php echo $house["postalcode"] ?></td>
                <td><?php echo $house["price"] ?></td>
                <td><?php echo $house["floor"] ?></td>
                <td class="price-m2"><?php echo ceil( $house["price"] / ($house["floor"] > 0 ? $house["floor"]:1) ) ?>m²</td>
                <td><?php echo $house["parcel"] ?></td>
                <td><?php echo $house["nbr_rooms"] ?></td>
                <td><?php echo $house["broker_name"] ?></td>
                <td><img src='<?php echo $house["hose_img"] ?>' width="100" /></td>
                <td><a href='<?php echo $house["hose_url"] ?>' target="_blank">Hose URL</a></td>
                <td><?php echo $house["status"] ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
