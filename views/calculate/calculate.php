<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>

<div class="container">

    <div class="stepwizard col-md-offset-3">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                <p>Step 1</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p>Step 2</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p>Step 3</p>
            </div>
        </div>
    </div>

    <form role="form" action="<?php echo Url::to(['calculate/post']); ?>" method="post">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <div class="row setup-content" id="step-1">
            <div class="col-xs-6 col-md-offset-3">
                <div class="col-md-12">
                    <h3> Step 1</h3>
                    <div class="form-group">
                        <label class="control-label">Postalcode (Four numbers and two letters, exemple: 9999AA)</label>
                        <input name="postalcode" pattern="[0-9][0-9][0-9][0-9][A-z][A-z]" type="text" required="required" class="form-control" placeholder="Postalcode"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Housenumber</label>
                        <input name="housenumber" pattern="\d+" type="text" required="required" class="form-control" placeholder="Housenumber" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Housenumber addition</label>
                        <input name="housenumberaddition" type="text" class="form-control" placeholder="Housenumber addition" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Square meter (floor)</label>
                        <input name="square_meter" pattern="\d+" type="text" required="required" class="form-control" placeholder="Square meter (floor)" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Reason</label>
                        <select name="reason" required="required" class="form-control">
                            <option value="0">i want to sell my house</option>
                            <option value="1">i want to buy my house</option>
                            <option value="2">i’m just curious about the price</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">In what time do you want to move</label>
                        <select name="tomove" required="required" class="form-control">
                            <option value="0">as fast as possible</option>
                            <option value="1">within 3 months</option>
                            <option value="2">within 6 month</option>
                            <option value="3">within 1 year</option>
                        </select>
                    </div>
                    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-2">
            <div class="col-xs-6 col-md-offset-3">
                <div class="col-md-12">
                    <h3> Step 2</h3>
                    <div class="form-group">
                        <label class="control-label">Type of house</label>
                        <select name="housetype" required="required" class="form-control">
                            <option value="0">Appartment</option>
                            <option value="1">Detached house</option>
                            <option value="2">Semi detached house</option>
                            <option value="3">Terraced house</option>
                            <option value="4">Corner house</option>
                        </select>
                    </div>
                    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-3">
            <div class="col-xs-6 col-md-offset-3">
                <div class="col-md-12">
                    <h3> Step 3</h3>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input name="email" type="email" required="required" class="form-control" placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">firstname</label>
                        <input name="firstname" type="text" required="required" class="form-control" placeholder="firstname" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">addition</label>
                        <input name="addition" type="text" required="required" class="form-control" placeholder="addition" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">lastname</label>
                        <input name="lastname" type="text" required="required" class="form-control" placeholder="lastname" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">phonenumber</label>
                        <input name="phonenumber" type="text" required="required" class="form-control" placeholder="phonenumber" />
                    </div>
                    <button class="btn btn-success btn-lg pull-right" type="submit">Calculate the value of my house</button>
                </div>
            </div>
        </div>
    </form>

</div>