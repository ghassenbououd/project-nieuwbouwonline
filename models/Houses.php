<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "houses".
 *
 * @property integer $house_id
 * @property string $streetname
 * @property string $housenumber
 * @property string $addition
 * @property string $city
 * @property string $postalcode
 * @property string $price
 * @property integer $floor
 * @property integer $parcel
 * @property integer $nbr_rooms
 * @property string $broker_name
 * @property string $broker_id
 * @property string $hose_img
 * @property string $hose_url
 * @property string $date_ins
 * @property string $date_upd
 * @property string $object_id
 * @property string $status
 */
class Houses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'houses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_id', 'streetname', 'housenumber', 'city', 'postalcode', 'price', 'floor', 'parcel', 'nbr_rooms', 'broker_name', 'broker_id', 'hose_img', 'hose_url', 'date_ins', 'date_upd', 'status'], 'required'],
            [['house_id', 'floor', 'parcel', 'nbr_rooms', 'object_id'], 'integer'],
            [['date_ins', 'date_upd'], 'safe'],
            [['streetname', 'city', 'price'], 'string', 'max' => 30],
            [['housenumber'], 'string', 'max' => 35],
            [['addition'], 'string', 'max' => 5],
            [['postalcode'], 'string', 'max' => 10],
            [['broker_name'], 'string', 'max' => 55],
            [['broker_id'], 'string', 'max' => 11],
            [['hose_img', 'hose_url'], 'string', 'max' => 150],
            [['status'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'house_id' => 'House ID',
            'streetname' => 'Streetname',
            'housenumber' => 'Housenumber',
            'addition' => 'Addition',
            'city' => 'City',
            'postalcode' => 'Postalcode',
            'price' => 'Price',
            'floor' => 'Floor',
            'parcel' => 'Parcel',
            'nbr_rooms' => 'Nbr Rooms',
            'broker_name' => 'Broker Name',
            'broker_id' => 'Broker ID',
            'hose_img' => 'Hose Img',
            'hose_url' => 'Hose Url',
            'date_ins' => 'Date Ins',
            'date_upd' => 'Date Upd',
            'object_id' => 'Object ID',
            'status' => 'Status',
        ];
    }
}
